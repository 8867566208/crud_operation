package Crud_Operation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Insert {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		String url = "jdbc:mysql://localhost:3306/test";
		String name = "root";
		String pass = "root";
	  Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection(url, name, pass);

		
		
		PreparedStatement ps = con.prepareStatement("insert into  employeee values (?,?,?);");
		ps.setInt(1, 5);
		ps.setString(2, "maithra");
		ps.setInt(3, 9500);
		
		int rows = ps.executeUpdate();
		
		if(rows>0)
		{
			System.out.println("INSERTED");
		}
		
		con.close();
	}

}
